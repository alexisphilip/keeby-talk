��    +      t  ;   �      �     �     �  "   �     �     
     !  	   '     1     =     D     W     ]     e     }  6   �  /   �     �       '     "   F     i     r     �  
   �     �     �     �     �     �     �     �     �     �     �     �  	                       $     8     G  _  U     �     �  #   �     �          )     1     :     L     [     r     y     �     �  1   �  8   �     )	     =	      N	  4   o	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     
     
     *
     ;
     >
     K
     Z
     g
     n
     }
     �
     �
     �
           +      (   	               &   "       $             '               *                       #           )                       !              %                    
                                  yet! Already have an account? Chose a least one related keyboard Content policy Don't have an account? Hello Keyboards Legal terms Log-in My recent activity Oops! Sign-in Tell us a great content Tell us a great title The one and only place to really talk about keyboards! The password must be at least 8 characters long The post's content The post's title There isn't any post(s) related to the  This functionality not working yet Welcome! activity-prefix activity-suffix created_on delete edit featured_keyboards follow keys log-in log[in] no copyright or post[create] post[update] posted on profile sign-in sign[in] total_related_posts update[avatar] update[email] Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-02-03 01:38+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 . Vous avez déjà un compte ? Choissiez un ou plusiers clavier(s) Politique de contenu Vous n'êtes pas inscrits ? Bonjour Claviers Mentions légales Connectez-vous Mon activité récente Oups ! Inscrivez-vous Dites-nous un bon 'contenu' Dites-nous un bon 'titre' Le seul et vrai endroit pour parler de claviers ! Le mot de passe doit être long d'au moins 8 caractères The contenu du post Le titre du post Il n'y a aucun post(s) liée au  Cette fonctionnalité n'est pas encore mise en place Bienvenue ! Activité récente de    crée_le supprimer modifier claviers_liés_au_post suivre touches Connectez-vous auth[connexion] pas de copyright ou post[créer] post[modifier] a posté sur profil inscrivez-vous auth[inscription] total_posts_clavier modifier[avatar] modifier[email] 