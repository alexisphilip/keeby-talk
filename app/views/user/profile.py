from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from app.models import Keyboard, Post


class ProfileView(LoginRequiredMixin, TemplateView):
    """Current's logged-in user profile's view."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    template_name = "user/profile.html"

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result["keebs"] = Keyboard.objects.all().order_by("name")
        result["posts"] = Post.objects.all().filter(author=self.request.user)
        result["title"] = "Me"
        return result
