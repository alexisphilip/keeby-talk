from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView

from app.models import Keyboard, Post


class UserDetailView(LoginRequiredMixin, DetailView):
    """User's profile view."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    template_name = "user/user_detail.html"
    model = User

    def get_slug_field(self):
        return "username"

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result["keebs"] = Keyboard.objects.all().order_by("name")
        result["posts"] = Post.objects.all().filter(author__username=self.kwargs["slug"])
        result["title"] = self.kwargs["slug"]
        return result
