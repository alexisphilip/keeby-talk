from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from app.models import Keyboard


class NotWorkingYetView(LoginRequiredMixin, TemplateView):
    """Opps! WIP."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    template_name = "other/not-working-yet.html"

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result["keebs"] = Keyboard.objects.all().order_by("name")
        result["title"] = "Oops!"
        return result
