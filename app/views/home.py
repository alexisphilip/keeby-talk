from django.views.generic import TemplateView

from app.models import Post, Keyboard


class HomeView(TemplateView):
    """Home view."""

    template_name = "home/home.html"

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)

        result["posts"] = Post.objects.all().order_by("-creation_date")
        result["keebs"] = Keyboard.objects.all().order_by("name")
        result["title"] = "Home"

        return result
