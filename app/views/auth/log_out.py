from django.contrib.auth import logout
from django.shortcuts import redirect


def logOutView(request):
    logout(request)
    return redirect("app_home")