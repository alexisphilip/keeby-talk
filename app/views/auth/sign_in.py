from django.shortcuts import render, redirect

from app.forms.sign_in import CreateUserForm


def signInView(request):
    """Sign-in view."""

    if request.user.is_authenticated:
        return redirect("app_home")
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("app_log_in")

        context = {
            "title": "Sign-in",
            "form": form}
        return render(request, "auth/sign-in.html", context)
