from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect


def logInView(request):
    """Log-in view."""

    if request.user.is_authenticated:
        return redirect("app_home")
    else:
        if request.method == "POST":
            username = request.POST.get("username")
            password = request.POST.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("app_home")
            else:
                context = {"error": "Username or password invalid."}
                return render(request, "auth/log-in.html", context)

        context = {"title": "Log-in"}
        return render(request, "auth/log-in.html", context)
