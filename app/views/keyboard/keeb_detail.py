from django.views.generic import DetailView

from app.models import Keyboard, Post


class KeebDetailView(DetailView):
    """Keeb detail view."""

    template_name = "keeb/keeb-detail.html"
    model = Keyboard

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)

        if self.kwargs.get("pk"):
            result["keyboard"] = Keyboard.objects.get(pk=self.kwargs.get("pk"))
            result["posts"] = Post.objects.filter(featured_keyboards__id=self.kwargs.get("pk"))
            result["title"] = result["keyboard"].name
            print(result["posts"])
        else:
            # TODO: if slug <pk> is None
            result["keyboard"] = None
            result["title"] = "Keyboard does not exist"
        return result
