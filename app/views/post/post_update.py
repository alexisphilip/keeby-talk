from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView

from app.models import Post, Keyboard


class PostUpdateView(LoginRequiredMixin, UpdateView):
    """Updates a post."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    # TODO: redirect to current's post after editing
    success_url = "/home"
    template_name = "post/post-update.html"
    model = Post
    fields = ["title", "content", "featured_keyboards"]

    # Overwriting query set to only allow the author to update the post.
    def get_queryset(self):
        queryset = super(PostUpdateView, self).get_queryset()
        queryset = queryset.filter(author=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)

        result["keebs"] = Keyboard.objects.all().order_by("name")

        # Truncates the title's string.
        title = self.object.title[:11] + "..." if len(self.object.title) > 14 else self.object.title
        result["title"] = "Edit: " + title

        return result
