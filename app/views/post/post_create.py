from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView

from app.models import Post, Keyboard


class PostCreateView(LoginRequiredMixin, CreateView):
    """Creates a post."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    success_url = "/home"
    template_name = "post/post-create.html"
    model = Post
    fields = ["title", "content", "featured_keyboards"]

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result["keebs"] = Keyboard.objects.all().order_by("name")
        result["title"] = "Create a post"
        return result
