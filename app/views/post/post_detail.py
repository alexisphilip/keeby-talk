from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView

from app.models import Post, Keyboard


class PostDetailView(LoginRequiredMixin, DetailView):
    """Post detail view."""

    login_url = "app_log_in"
    redirect_field_name = "app_log_in"

    template_name = "post/post-detail.html"
    model = Post

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)

        result["keebs"] = Keyboard.objects.all().order_by("name")

        """
        if self.object.author.pk == self.request.user.id:
            result["is_author"] = True
        else:
            result["is_author"] = False
        """

        # Truncates the title's string.
        title = self.object.title[:17] + "..." if len(self.object.title) > 20 else self.object.title
        result["title"] = title
        return result
