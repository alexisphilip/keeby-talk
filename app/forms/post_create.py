from django import forms


class PostCreateForm(forms.Form):

    attributes = {
        'class': 't-input'
    }

    title = forms.CharField(widget=forms.TextInput(
        attrs=attributes), required=True)
    content = forms.CharField(widget=forms.TextInput(
        attrs=attributes), required=True)
