from django import forms


class LoginForm(forms.Form):
    user_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 't-input'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 't-input'}))
