from django.contrib import admin

from app.models import Person, Keyboard, Post

admin.site.register(Person)
admin.site.register(Keyboard)
admin.site.register(Post)
