from django.contrib.auth.models import User
from django.db import models
from django.db.models import PROTECT
from django.utils.timezone import now


class Person(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)

    birth_date = models.DateField(blank=False,
                                  null=False,
                                  default=None)

    description = models.TextField(blank=True,
                                   null=True,
                                   default=None)

    """
    def __str__(self):
        return '{} ({})'.format(
            self.user,
            ' / '.join([str(c) for c in self.keyboards.all()])
        )
    """


class Keyboard(models.Model):
    name = models.TextField(blank=False,
                            null=False,
                            default=None)

    description = models.TextField(blank=False,
                                   null=False,
                                   default=None)

    keys = models.IntegerField(null=True,
                               default=None)

    release_date = models.DateField(null=True,
                                    default=None)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.TextField(blank=False,
                             null=False,
                             default=None)

    content = models.TextField(blank=False,
                               null=False,
                               default=None)

    creation_date = models.DateTimeField(default=now)

    author = models.ForeignKey(User,
                               null=False,
                               default=1,
                               on_delete=PROTECT)

    featured_keyboards = models.ManyToManyField(Keyboard,
                                                default=1)

    def __str__(self):
        return self.title
