# Generated by Django 3.0 on 2020-02-02 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20200202_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='featured_keyboards',
            field=models.ManyToManyField(default=1, to='app.Keyboard'),
        ),
    ]
