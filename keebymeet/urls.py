"""keebymeet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

# Base
from app.views import home

# Users
from app.views.user import (
    profile,
    user_detail)

# Authentication
from app.views.auth import (
    log_out,
    sign_in,
    log_in)

# Posts
from app.views.post import (
    post_create,
    post_detail,
    post_update)

# Keyboards
from app.views.keyboard import keeb_detail

# Other
from app.views.other import not_working_yet

urlpatterns = [
    path("admin/", admin.site.urls)
]

urlpatterns += i18n_patterns(
    # Home
    path("", home.HomeView.as_view(), name="app_home"),
    path("home/", home.HomeView.as_view(), name="app_home"),

    # Authentication
    path("sign-in/", sign_in.signInView, name="app_sign_in"),
    path("log-in/", log_in.logInView, name="app_log_in"),
    path("log-out/", log_out.logOutView, name="app_log_out"),

    # Users
    path("profile/", profile.ProfileView.as_view(), name="app_profile"),
    path("user/<str:slug>", user_detail.UserDetailView.as_view(), name="app_user_detail"),

    # Posts
    path("post/create/", post_create.PostCreateView.as_view(), name="app_post_create"),
    path("post/<int:pk>", post_detail.PostDetailView.as_view(), name="app_post_detail"),
    path("post/update/<int:pk>", post_update.PostUpdateView.as_view(), name="app_post_update"),

    # Keyboards
    path("keeb/<int:pk>", keeb_detail.KeebDetailView.as_view(), name="app_keeb_detail"),

    # Others
    path("NotWorkingYetView", not_working_yet.NotWorkingYetView.as_view(), name="app_not_working_yet")
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
